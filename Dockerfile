FROM node:16.13.1
WORKDIR /app
COPY ["package.json", "package-lock.json*", "./"]
RUN yarn
COPY . .
CMD ["npm", "start"]