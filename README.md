# NFTs on the Swarm Network

Mint and view NFTs (ERC-721) that are stored on the Swarm network. You can choose to mint these NFTs on any EVM compatible blockchain, and store the NFT data on the Swarm network.

## Project

This project is a proof-of-concept (PoC) on how NFT platforms would work on the Swarm network as the decentralized data storage.

It consists of 3 parts:
* Blockchain contracts (solidity)
* Backend (Node.JS)
* Frontend (Handlebars)

The blockchain part is located in the `contracts` folder. It includes a smart contract that needs to be deployed on an EVM chain. This smart contract will mint and handle the transferring of the NFTs between users. If you want to run this application yourself, you will need to deploy that smart contract on any EVM chain, or you could just reuse the one I deployed on Goerli (`0x12d963E80cbfEf4ADd105aeC18DE1317459F6B79`). You can deploy the contract using Remix.

The backend will handle uploading the NFT image and the NFT's metadata to the Swarm network using the Bee node running in this dockerized environment. The backend will also feature some API endpoints which the frontend will use to fetch NFTs, metadata and their images from Swarm.
The backend uses [bee-js](https://bee-js.ethswarm.org/docs/) to communicate and interact with the Bee node.

The frontend is a one-pager app which allows you to connect your Metamask wallet and mint and view the NFTs, built with the Handlebars templating engine.

Please note the focus of this project is not necessarily on the dashboard/frontend, but more about demonstrating how Swarm decentralized storage can be used to complete the current decentralized infrastructure, like NFT tokens.

## Installation and Running

1. Deploy your own copy of `SwarmNft.sol` using remix, or use the one I have deployed already on Goerli: `0x12d963E80cbfEf4ADd105aeC18DE1317459F6B79`
2. Copy `.env.example` into `.env` and input the relevant data:
    * `JSON_RPC` - EVM JSON RPC (you can use the JSON RPC for Ethereum Goerli testnet from Infura or Alchemy)
    * `SWARMNFT_ADDRESS` - The address of the `SwarmNft.sol` contract deployed (NOTE: It must be on the same chain that the JSON RPC above is working on)
    * `SWARM_POSTAGE_BATCH_ID` - The Swarm Postage Batch ID used for uploading to the Swarm Network.
    * `NODE_ENV` - Environment (just use `production`)
    * For all other `BEE SETTINGS` please refer to the [swarm docs](https://docs.ethswarm.org/docs/installation/docker/). I am running the Bee on testnet.
3. Fund the Swarm address with gETH and gBZZ.
4. Run the docker compose file

```
docker-compose up --build
```

4. You can now access your app at `http://127.0.0.1:1337`

## Live Demo Link

I have a live demo running which is deployed on a Digital Ocean droplet. It uses Goerli as testnet for both the NFTs and Swarm. NFT contract address: `0x12d963E80cbfEf4ADd105aeC18DE1317459F6B79`. You can check it our here: http://206.189.249.30/

## Demo Video

https://youtu.be/sVR96E3kXkE