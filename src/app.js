require('dotenv').config();
const express = require('express');
const multer = require('multer');
const handlebars = require('express-handlebars');
var bodyParser = require('body-parser');
const Network = require('./networks');
const engine = handlebars.engine;
const fs = require('fs');
const sharp = require('sharp');

var app = express();

const upload = multer({
    dest: "/tmp"
});

app.use(express.static('static'));
app.use(bodyParser.urlencoded({ extended: true })); 
app.engine('handlebars', engine());
app.set('view engine', 'handlebars');
app.set('views', './views');

app.get('/', (req, res) => {
    res.render('home');
});

// API

app.get('/api/allTokens', async (req, res) => {
    const NFTs = await Network.getNFTs();
    res.status(200).send(NFTs);
});

app.get('/api/nftContractAddress', async (req, res) => {
    res.status(200).send(process.env.SWARMNFT_ADDRESS);
});

app.post('/api/uploadNFT', upload.single('image'), async (req, res) => {

    try {

        if (typeof(req.file) == 'undefined' || typeof(req.body) == 'undefined') {
            res.status(500).send("Invalid params 1");
            return;
        }

        const tempPath = req.file.path;
        const nftName = req.body.name;

        if (typeof(tempPath) == 'undefined' || typeof(nftName) == 'undefined') {
            res.status(500).send("Invalid params 2");
            return;
        }

        if (nftName.length > 15) {
            res.status(500).send("Name too long");
            return;
        }
        
        const f = fs.readFileSync(tempPath).toString('hex');
        const fbuff = f.slice(0, 6);
        if (fbuff != "ffd8ff") {
            res.status(500).send("Invalid JPEG");
            console.log(fbuff)
            return;
        }

        // Resize image
        const lPath = "/tmp/lastNFT"
        await sharp(tempPath).resize(256, 256, {fit: "fill"}).toFile(lPath);

        const nftHash = await Network.uploadNFT(lPath, nftName);
        res.status(200).send(nftHash);
    } catch(e) {
        res.status(500).send(e.toString())
    }
})

app.get('/api/rawSwarmImage/:sHash', async (req, res) => {
    const h = req.params.sHash;
    const image = await Network.getRawSwarmImage(h);
    res.contentType('image/jpeg');
    res.end(image, 'binary');
});

app.listen(1337, () => {
    console.log("App listening on http://0.0.0.0:1337")
})