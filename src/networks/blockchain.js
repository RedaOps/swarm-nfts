const ethers = require('ethers');
const SwarmNftAbi = require('../../contracts/SwarmNft.abi.json');
const SwarmNftInterface = new ethers.utils.Interface(
    JSON.parse(JSON.stringify(SwarmNftAbi))
);

const provider = new ethers.providers.JsonRpcProvider(process.env.JSON_RPC);
const SwarmNFT = new ethers.Contract(
    process.env.SWARMNFT_ADDRESS,
    SwarmNftInterface,
    provider
);

const getLastTokenIndex = async () => {
    const lastTokenIndex = await SwarmNFT.currentTokenId();
    return lastTokenIndex;
}

const getSwarmHashes = async () => {
    const lastIndex = await getLastTokenIndex();
    var hashes = {}
    for(var i = 1; i <= lastIndex; i++) {
        var h = await SwarmNFT.getTokenSwarmHash(i);
        hashes[i] = h;
    }

    return hashes;
}

const getTokenOwner = async (tokenId) => {
    const owner = await SwarmNFT.ownerOf(tokenId);
    return owner;
}

const getNFTSwarmHash = async (tokenId) => {
    const swarmHash = await SwarmNFT.getTokenSwarmHash(tokenId);
    return swarmHash;
}

module.exports = {
    getSwarmHashes,
    getLastTokenIndex,
    getNFTSwarmHash,
    getTokenOwner
}