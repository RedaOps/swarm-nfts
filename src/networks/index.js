const Swarm = require('./swarm');
const Blockchain = require('./blockchain');

var CACHED_NFTS = {
    total: 0,
    nfts: []
}

const uploadNFT = async (imagePath, name) => {
    // Upload image
    const imageResult = await Swarm.uploadImageByPath(imagePath);
    const nftData = {
        name: name,
        image: imageResult
    }

    const nftHash = await Swarm.uploadJson(nftData);
    return nftHash;
}

const getRawSwarmImage = async (sHash) => {
    const data = await Swarm.getImage(sHash);
    return data;
}

const getNFTs = async () => {
    const lastIndex = await Blockchain.getLastTokenIndex();
    if (CACHED_NFTS.total < lastIndex) {
        for(var i = CACHED_NFTS.total+1; i <= lastIndex; i++) {
            var nftSwarmHash = await Blockchain.getNFTSwarmHash(i);
            var [name, imageHash] = await Swarm.fetchNFTMetadata(nftSwarmHash);
            var owner = await Blockchain.getTokenOwner(i);
            CACHED_NFTS.total += 1;
            CACHED_NFTS.nfts.push({
                index: i,
                name: name,
                imageURI: `/api/rawSwarmImage/${imageHash}`,
                owner: owner
            });
        }
    }
    return CACHED_NFTS;
}

module.exports = {
    uploadNFT,
    getRawSwarmImage,
    getNFTs
}