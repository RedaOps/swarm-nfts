const beejs = require('@ethersphere/bee-js');
const fs = require("fs");
const bee = new beejs.Bee("http://bee-1:1633");

const uploadJson = async (data) => {
    const result = await bee.uploadFile(process.env.SWARM_POSTAGE_BATCH_ID, JSON.stringify(data));
    return result.reference;
}

const uploadImage = async (data) => {
    const result = await bee.uploadFile(process.env.SWARM_POSTAGE_BATCH_ID, data, {type: 'image/jpeg'});
    return result.reference;
}

const uploadImageByPath = async (path) => {
    const img = fs.readFileSync(path);
    const result = await bee.uploadFile(process.env.SWARM_POSTAGE_BATCH_ID, img, "image.jpg", {type: 'image/jpeg'});
    return result.reference;
}

const getImage = async (sHash) => {
    const result = await bee.downloadFile(sHash);
    return result.data;
}

const fetchNFTMetadata = async (swarmHash) => {
    const data = await bee.downloadFile(swarmHash);
    const jsonData = data.data.json();

    return [jsonData.name, jsonData.image];
}

module.exports = {
    uploadJson,
    getImage,
    uploadImage,
    uploadImageByPath,
    fetchNFTMetadata
}