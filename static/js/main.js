var SIGNER = {}
var NFTS = {}

async function readFileAsDataURL(file) {
    let result_base64 = await new Promise((resolve) => {
        let fileReader = new FileReader();
        fileReader.onload = (e) => resolve(fileReader.result);
        fileReader.readAsDataURL(file);
    });

    console.log(result_base64); // aGV5IHRoZXJl...

    return result_base64;
}

const fetchNFTs = async () => {

    return new Promise((resolve) => {
        try {
            var xhr = new XMLHttpRequest();

            xhr.onload = () => {
                if (xhr.status != 200) {
                    throw Error("API not 200");
                }
                NFTS = JSON.parse(xhr.responseText);
                resolve(true);
            }
            xhr.open("get", "/api/allTokens", true);
            xhr.send();
        } catch(e) {
            alertify.error("Fetching NFTs failed");
            resolve(true);
        }
    });
}

const fetchNFTContractAddress = async () => {
    var xhr = new XMLHttpRequest();
    xhr.open("get", '/api/nftContractAddress', false);
    xhr.send();

    return xhr.responseText;
}

const submitNFTMetadata = async () => {

    var form = document.getElementById("NFT_FORM");
    var data = new FormData(form);

    var xhr  = new XMLHttpRequest();
    xhr.open("post", "/api/uploadNFT", false);
    xhr.send(data);

    if (xhr.status == 200) {
        var sHash = xhr.responseText;
        return sHash;
        // Metamask NFT
    } else {
        console.log(xhr.responseText);
        alertify.error("Swarm Upload Failed");
        return null;
    }
}

const updateNFTPreview = async () => {
    var image = document.getElementById("nft_image").files[0];
    var data = await readFileAsDataURL(image);
    document.getElementById("nft_preview").src = data;
}

const connectMetamask = async () => {
    if (window.ethereum) {
        const provider = new ethers.providers.Web3Provider(window.ethereum)

        await provider.send("eth_requestAccounts", []);

        signer = await provider.getSigner();
        var addr = await signer.getAddress();

        // Change button

        document.getElementById("nft_mint_address").value = addr;
        document.getElementById("nft_form_button").onclick = submitNFT;
        document.getElementById("nft_form_button").innerHTML = "Mint Swarm NFT";
        document.getElementById("nft_form_button").classList.add("btn-success");
    } else {
        alertify.error("No Metamask Wallet detected");
    }
}

const submitNFT = async () => {
    try {
        const mintAbi = [
            "function mintSwarmNft(string memory _swarmReference) external returns (uint256)"
        ]

        const contractAddress = await fetchNFTContractAddress();

        const contract = new ethers.Contract(
            contractAddress,
            mintAbi,
            signer
        );

        const swarmHash = await submitNFTMetadata();
        if (swarmHash == null) {
            return;
        }

        // Loading animation
        const r = await contract.mintSwarmNft(swarmHash);
        alertify.warning("Confirming... Please Wait...")
        await r.wait();

        // Display NFT

        alertify.success("NFT Minted. Refreshing the page...")
        setTimeout(() => {
            location.reload();
        }, 5000);
    } catch(e) {
        alertify.error("Error minting NFT: "+e);
    }
}

const toggleNFTsLoading = () => {
    const loader = document.getElementById("nfts-loading");
    if (loader.style.display == "block")
        loader.style.display = "none"
    else
        loader.style.display = "block"
}

const openTokenExplorerWindow = async (tokenId) => {
    var nftAddress = await fetchNFTContractAddress();
    var url = `https://goerli.etherscan.io/token/${nftAddress}?a=${tokenId}`
    window.open(url);
}

function encodeHTML(s) {
    return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
}

const displayAddress = (address) => {
    return `${address.substring(0, 4)} ... ${address.substring(39)}`;
};

const buildGallery = async () => {
    const parentDiv = document.getElementById("nft-gallery");
    var itemsToAdd = [];
    for(var i = 0; i < NFTS.total; i++) {
        var newElement = document.createElement('div');
        newElement.classList = ['col-md-3 nft-element'];

        var img = document.createElement('img');
        img.src = NFTS.nfts[i].imageURI;
        eval("img.onclick = () => { openTokenExplorerWindow("+(i+1).toString()+") }")
        img.classList = 'nft-preview click-me';
        newElement.appendChild(img);

        var brElement = document.createElement("br");
        newElement.appendChild(brElement)

        var title = document.createElement("figcaption")
        title.innerHTML = "<b>"+encodeHTML(NFTS.nfts[i].name)+"</b>"; // Weak XSS protection, but this is just a PoC
        newElement.appendChild(title)

        var idx = document.createElement("small");
        idx.classList = 'text-muted';
        idx.innerHTML = `Swarm NFT #${i+1}`
        newElement.appendChild(idx)
        newElement.appendChild(brElement)

        var ownerInfo = document.createElement("small")
        ownerInfo.classList = 'text-muted';
        ownerInfo.innerHTML = `Owned by <a target="_blank" href="https://goerli.etherscan.io/address/${NFTS.nfts[i].owner}">${displayAddress(NFTS.nfts[i].owner)}</a>`
        newElement.appendChild(ownerInfo)

        itemsToAdd.push(newElement);
        if (i+1 == NFTS.total || itemsToAdd.length == 4) {
            var newRow = document.createElement("div");
            newRow.classList = ['row'];
            for(var j = 0; j < itemsToAdd.length; j++) {
                newRow.appendChild(itemsToAdd[j]);
            }
            parentDiv.appendChild(newRow);
            parentDiv.append(document.createElement("br"));
            itemsToAdd = [];
        }
    }
    parentDiv.style.display = "block";
}

const mountPoint = async () => {
    document.getElementById("nft-gallery").style.display = "none";
    toggleNFTsLoading();
    await fetchNFTs();
    if (NFTS.total == 0) {
        document.getElementById("nft-gallery").style.display = "none";
        document.getElementById("no-nft-text").style.display = "block";
    } else {
        await buildGallery();
    }
    toggleNFTsLoading();
}

mountPoint();